# Basic Example

Basic VPC provision example which uses the output from the acl-profile module as the value for var.acl_rules_map.
The example also adds the ibm-ntp-server VPE (virtual private endpoint) to be added to transit zone.
