terraform {
  required_version = ">= 1.0.0"
  experiments      = [module_variable_optional_attrs]
  required_providers {
    ibm = {
      source = "ibm-cloud/ibm"
      # Lock into lowest version in example to ensure we don't end up with invalid lower range
      version = "1.36.0"
    }
  }
}
