# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# These parameters have reasonable descriptions.
# ---------------------------------------------------------------------------------------------------------------------

variable "ibmcloud_api_key" {
  description = "APIkey that's associated with the account to use, set via environment variable TF_VAR_ibmcloud_api_key"
  type        = string
  sensitive   = true
}

variable "prefix" {
  description = "Display name of the VPC and prefix for related resources"
  type        = string
  default     = "example-vpc"

}
variable "region" {
  description = "Name of the Region to deploy into"
  type        = string
  default     = "us-south"
}
