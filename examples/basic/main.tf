# Create the Resource Group used in this example
resource "ibm_resource_group" "resource_group" {
  name     = "${var.prefix}-resource-group"
  quota_id = null
}

# ACL profile
module "profile" {
  source = "git::ssh://git@github.ibm.com/GoldenEye/acl-profile-ocp.git?ref=1.0.14"
}

# VPC
module "vpc" {
  source            = "../.."
  unique_name       = var.prefix
  ibm_region        = var.region
  resource_group_id = ibm_resource_group.resource_group.id
  acl_rules_map = {
    private = concat(module.profile.base_acl, module.profile.https_acl, module.profile.deny_all_acl)
  }
  virtual_private_endpoints = {
    ntp : "ibm-ntp-server"
  }
}
