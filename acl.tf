
##############################################################################
# Create an  ACL for ingress/egress used by  all subnets in VPC
##############################################################################

locals {
  allow_subnet_cidr_inbound_rules = [
    for k, v in module.zone_subnet_addrs :
    {
      name        = "allow-traffic-subnet-${k}-inbound"
      action      = "allow"
      source      = v.base_cidr_block
      destination = "0.0.0.0/0"
      direction   = "inbound"
    }
  ]
  allow_subnet_cidr_outbound_rules = [
    for k, v in module.zone_subnet_addrs :
    {
      name        = "allow-traffic-subnet-${k}-outbound"
      action      = "allow"
      source      = "0.0.0.0/0"
      destination = v.base_cidr_block
      direction   = "outbound"
    }
  ]
  acl_rules = flatten(
    [
      local.allow_subnet_cidr_inbound_rules,
      local.allow_subnet_cidr_outbound_rules
    ]
  )
}

resource "ibm_is_network_acl" "multizone_acl" {
  for_each       = var.acl_rules_map
  name           = "${var.unique_name}-${each.key}-2-others-acl"
  vpc            = ibm_is_vpc.vpc.id
  resource_group = var.resource_group_id

  # Create ACL rules
  dynamic "rules" {
    for_each = flatten([local.acl_rules, each.value])
    content {
      name        = rules.value.name
      action      = rules.value.action
      source      = rules.value.source
      destination = rules.value.destination
      direction   = rules.value.direction

      ##############################################################################
      # Dynamically create TCP rules
      ##############################################################################

      dynamic "tcp" {

        # Runs a for each loop, if the rule block contains tcp, it looks through the block
        # Otherwise the list will be empty

        for_each = (
          contains(keys(rules.value), "tcp")
          ? rules.value.tcp[*]
          : []
        )

        # Conditionally adds content if sg has tcp
        content {
          port_min        = rules.value.tcp.port_min
          port_max        = rules.value.tcp.port_max
          source_port_min = rules.value.tcp.source_port_min
          source_port_max = rules.value.tcp.source_port_max
        }
      }

      ##############################################################################

      ##############################################################################
      # Dynamically create UDP rules
      ##############################################################################

      dynamic "udp" {

        # Runs a for each loop, if the rule block contains udp, it looks through the block
        # Otherwise the list will be empty

        for_each = (
          contains(keys(rules.value), "udp")
          ? rules.value.udp[*]
          : []
        )

        # Conditionally adds content if sg has udp
        content {
          port_min        = rules.value.udp.port_min
          port_max        = rules.value.udp.port_max
          source_port_min = rules.value.udp.source_port_min
          source_port_max = rules.value.udp.source_port_max
        }
      }

      ##############################################################################

      ##############################################################################
      # Dynamically create ICMP rules
      ##############################################################################

      dynamic "icmp" {

        # Runs a for each loop, if the rule block contains icmp, it looks through the block
        # Otherwise the list will be empty

        for_each = (
          contains(keys(rules.value), "icmp")
          ? rules.value.icmp[*]
          : []
        )

        # Conditionally adds content if sg has icmp
        content {
          type = rules.value.icmp.type
          code = rules.value.icmp.code
        }
      }

      ##############################################################################

    }
  }
}

##############################################################################
