# VPC Module

<!-- BADGE UPDATES:
1. Update first badge below to the current status of the module. See options at
    https://github.ibm.com/GoldenEye/documentation/blob/master/status.md
2. Update the build status badge to point to the travis pipeline for the module
    TIP: Simply replace the string "module-template" in the 2 places in the Build Status section below
-->

[![Graduated](https://img.shields.io/badge/Status-Graduated%20(Beta)-green)](https://github.ibm.com/GoldenEye/documentation/blob/master/status.md) [![Build Status](https://travis.ibm.com/GoldenEye/vpc-module.svg?token=3Ry6sEDNvWajQPuZHgTZ&branch=master)](https://travis.ibm.com/GoldenEye/vpc-module) [![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release) [![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit) [![latest release](https://shields-server.m03l6u0cqkx.eu-de.codeengine.appdomain.cloud/github/v/release/GoldenEye/vpc-module?logo=GitHub)](https://github.ibm.com/GoldenEye/vpc-module/releases/latest) [![Catalog release](https://img.shields.io/badge/release-IBM%20Cloud%20Catalog-3662FF?logo=ibm)](https://cloud.ibm.com/catalog/content/goldeneye-vpc-module-9aa9f92c-550a-402a-92ff-a71aa12564f1-global)

This module implements a double DMZ design, where the actual workload is isolated into a (private) subnet, and all traffic to public networks goes through a distinct (edge) subnet. Traffic to other IBM services is routed through the transit network.

## Compliance and security

This module implements the following NIST controls on the network layer. For more information about how this module implements the controls in the following list, see [NIST controls](docs/controls.md).

| Profile | Category | ID       | Description |
|---------|----------|----------|-------------|
| NIST    | AC-4     | AC-4(2)  | Implement protected processing domains. VPCs and subnets are separate processing domains with distinct security attributes. |
| NIST    | AC-4     | AC-4(4)  | Implement flow control of encrypted information. Future versions might include deep inspection for traffic at edge and transit networks. |
| NIST    | AC-4     | AC-4(6)  | Enforce flow control through defined metadata. The egress configuration serves as the metadata. Unless allowed by the egress gateway, no flow is possible. |
| NIST    | AC-6     | AC-6(4)  | Implement separate processing domains. See AC-4(2). |
| NIST    | SC-5     | SC-5(1)  | Restrict the ability to attack other systems. Public and private ingress is separated by subnet. |
| NIST    | SC-5     | SC-5(2)  | Manage capacity, bandwidth, and redundancy. Attacks on public networks cannot affect private endpoints. |
| NIST    | SC-5     | SC-5(3)  | Implement detection and monitoring. Future versions might use flow logs for monitoring and anomaly detection. |
| NIST    | SC-7     | SC-7(3)  | Limit the number of external network connections to the system. |
| NIST    | SC-7     | SC-7(4)  | Implement egress and ingress gateways as a control point for external services. |
| NIST    | SC-7     | SC-7(5)  | Deny network traffic by default and allow through egress configuration. The workload cannot make external connections. |
| NIST    | SC-7     | SC-7(8)  | Route traffic to authenticated proxy servers. Traffic to public must traverse the egress gateway at the edge. |
| NIST    | SC-7     | SC-7(18) | Fail securely for operational failures of a boundary protection device. A Calico failure does not lead to unintended system behavior. |
| NIST    | SC-7     | SC-7(20) | (Requires multiple instances). Implement dynamic isolation capability. Use multiple VPC module instances to isolate the control plane and data plane. |
| NIST    | SC-7     | SC-7(21) | (Requires multiple instances). Isolate system components through multiple independent control planes or data planes. See SC-7(20). |
| NIST    | SC-7     | SC-7(22) | Implement separate subnets to connect to different security domains in a double DMZ design.<br/>With multiple instances of this module: Isolate the control plane and data plane into different subnets. Except for subnets that are dedicated to brokering traffic to other systems, similar workloads are separated into subnets that cannot reach other subnets. |

## Reference architecture

![Service VPC](./docs/VPCv2.0.png)

## Usage
```hcl
# Replace "master" with a GIT release version to lock into a specific release
module "vpc-module" {
  source = "https://cm.globalcatalog.cloud.ibm.com/api/v1-beta/offering/source?archive=tgz&kind=terraform&name=goldeneye-vpc-module&version=2.2.0"
  unique_name       = "example-name"
  ibm_region        = "us-south"
  # modify the value for resource_group_id with and id of a group you own
  resource_group_id = "id of existing resource group"
  virtual_private_endpoints = {}
  # customize the rules below for your specific needs
  acl_rules_map = {  private = [
      {
        name        = "iks-create-worker-nodes-inbound"
        action      = "allow"
        source      = "161.26.0.0/16"
        destination = "0.0.0.0/0"
        direction   = "inbound"
      },
      {
        name        = "iks-nodes-to-master-inbound"
        action      = "allow"
        source      = "166.8.0.0/14"
        destination = "0.0.0.0/0"
        direction   = "inbound"
      },
      {
        name        = "iks-create-worker-nodes-outbound"
        action      = "allow"
        source      = "0.0.0.0/0"
        destination = "161.26.0.0/16"
        direction   = "outbound"
      },
      {
        name        = "iks-worker-to-master-outbound"
        action      = "allow"
        source      = "0.0.0.0/0"
        destination = "166.8.0.0/14"
        direction   = "outbound"
      },
      {
        name        = "allow-all-https-inbound"
        source      = "0.0.0.0/0"
        action      = "allow"
        destination = "0.0.0.0/0"
        direction   = "inbound"
        tcp = {
          source_port_min = 443
          source_port_max = 443
          port_min        = 1
          port_max        = 65535
        }
      },
      {
        name        = "allow-all-https-outbound"
        source      = "0.0.0.0/0"
        action      = "allow"
        destination = "0.0.0.0/0"
        direction   = "outbound"
        tcp = {
          source_port_min = 1
          source_port_max = 65535
          port_min        = 443
          port_max        = 443
        }
      },
      {
        name        = "deny-all-outbound"
        action      = "deny"
        source      = "0.0.0.0/0"
        destination = "0.0.0.0/0"
        direction   = "outbound"
      },
      {
        name        = "deny-all-inbound"
        action      = "deny"
        source      = "0.0.0.0/0"
        destination = "0.0.0.0/0"
        direction   = "inbound"
      }
    ]
  }
}
```

## Required IAM access policies
You need the following permissions to run this module.

- Account Management
  - **Resource Group** service
      - `Viewer` platform access
- IAM Services
  - **VPC Infrastructure** service
      - `Editor` platform access

For more information about the access you need to run all the GoldenEye modules, see [GoldenEye IAM permissions](https://github.ibm.com/GoldenEye/documentation/blob/master/goldeneye-iam-permissions.md).

## Examples

[Basic Example](examples/basic)

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.0 |
| <a name="requirement_ibm"></a> [ibm](#requirement\_ibm) | >= 1.36.0 |
| <a name="requirement_null"></a> [null](#requirement\_null) | >= 3.1.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_vpc_subnets"></a> [vpc\_subnets](#module\_vpc\_subnets) | ./subnet | n/a |
| <a name="module_zone_subnet_addrs"></a> [zone\_subnet\_addrs](#module\_zone\_subnet\_addrs) | git::https://github.com/hashicorp/terraform-cidr-subnets.git | v1.0.0 |

## Resources

| Name | Type |
|------|------|
| [ibm_is_network_acl.multizone_acl](https://registry.terraform.io/providers/ibm-cloud/ibm/latest/docs/resources/is_network_acl) | resource |
| [ibm_is_public_gateway.gateway](https://registry.terraform.io/providers/ibm-cloud/ibm/latest/docs/resources/is_public_gateway) | resource |
| [ibm_is_security_group_rule.secure_cluster_sg_rules](https://registry.terraform.io/providers/ibm-cloud/ibm/latest/docs/resources/is_security_group_rule) | resource |
| [ibm_is_virtual_endpoint_gateway.endpoint_gateways](https://registry.terraform.io/providers/ibm-cloud/ibm/latest/docs/resources/is_virtual_endpoint_gateway) | resource |
| [ibm_is_vpc.vpc](https://registry.terraform.io/providers/ibm-cloud/ibm/latest/docs/resources/is_vpc) | resource |
| [null_resource.subnet_mappings](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_acl_rules_map"></a> [acl\_rules\_map](#input\_acl\_rules\_map) | Access control list rule set per network zone | <pre>map(list(object({<br>    name        = string<br>    action      = string<br>    source      = string<br>    destination = string<br>    direction   = string<br>    tcp = optional(object({<br>      source_port_min = number<br>      source_port_max = number<br>      port_min        = number<br>      port_max        = number<br>    }))<br>    udp = optional(object({<br>      source_port_min = number<br>      source_port_max = number<br>      port_min        = number<br>      port_max        = number<br>    }))<br>    icmp = optional(object({<br>      type = number<br>      code = number<br>    }))<br>  })))</pre> | <pre>{<br>  "private": [<br>    {<br>      "action": "allow",<br>      "destination": "0.0.0.0/0",<br>      "direction": "inbound",<br>      "name": "iks-create-worker-nodes-inbound",<br>      "source": "161.26.0.0/16"<br>    },<br>    {<br>      "action": "allow",<br>      "destination": "0.0.0.0/0",<br>      "direction": "inbound",<br>      "name": "iks-nodes-to-master-inbound",<br>      "source": "166.8.0.0/14"<br>    },<br>    {<br>      "action": "allow",<br>      "destination": "161.26.0.0/16",<br>      "direction": "outbound",<br>      "name": "iks-create-worker-nodes-outbound",<br>      "source": "0.0.0.0/0"<br>    },<br>    {<br>      "action": "allow",<br>      "destination": "166.8.0.0/14",<br>      "direction": "outbound",<br>      "name": "iks-worker-to-master-outbound",<br>      "source": "0.0.0.0/0"<br>    },<br>    {<br>      "action": "allow",<br>      "destination": "0.0.0.0/0",<br>      "direction": "inbound",<br>      "name": "allow-all-https-inbound",<br>      "source": "0.0.0.0/0",<br>      "tcp": {<br>        "port_max": 65535,<br>        "port_min": 1,<br>        "source_port_max": 443,<br>        "source_port_min": 443<br>      }<br>    },<br>    {<br>      "action": "allow",<br>      "destination": "0.0.0.0/0",<br>      "direction": "outbound",<br>      "name": "allow-all-https-outbound",<br>      "source": "0.0.0.0/0",<br>      "tcp": {<br>        "port_max": 443,<br>        "port_min": 443,<br>        "source_port_max": 65535,<br>        "source_port_min": 1<br>      }<br>    },<br>    {<br>      "action": "deny",<br>      "destination": "0.0.0.0/0",<br>      "direction": "outbound",<br>      "name": "deny-all-outbound",<br>      "source": "0.0.0.0/0"<br>    },<br>    {<br>      "action": "deny",<br>      "destination": "0.0.0.0/0",<br>      "direction": "inbound",<br>      "name": "deny-all-inbound",<br>      "source": "0.0.0.0/0"<br>    }<br>  ]<br>}</pre> | no |
| <a name="input_cidr_bases"></a> [cidr\_bases](#input\_cidr\_bases) | A list of base CIDR blocks for each network zone | `map(string)` | <pre>{<br>  "edge": "192.168.32.0/20",<br>  "nlb": "192.168.48.0/20",<br>  "private": "192.168.0.0/20",<br>  "transit": "192.168.16.0/20"<br>}</pre> | no |
| <a name="input_classic_access"></a> [classic\_access](#input\_classic\_access) | Enable VPC Classic Access. Note: only one VPC per region can have classic access | `bool` | `false` | no |
| <a name="input_enable_public_gateway"></a> [enable\_public\_gateway](#input\_enable\_public\_gateway) | Enable public gateways for private subnets, true or false | `bool` | `true` | no |
| <a name="input_ibm_region"></a> [ibm\_region](#input\_ibm\_region) | IBM Cloud region where all resources will be deployed | `string` | n/a | yes |
| <a name="input_new_bits"></a> [new\_bits](#input\_new\_bits) | Number of additional address bits to use for numbering the new networks | `number` | `2` | no |
| <a name="input_resource_group_id"></a> [resource\_group\_id](#input\_resource\_group\_id) | ID of resource group to use when creating the VPC | `string` | n/a | yes |
| <a name="input_security_group_rules"></a> [security\_group\_rules](#input\_security\_group\_rules) | List of security group rules to be added to default security group | <pre>map(object({<br>    source    = string<br>    direction = string<br>    tcp = optional(object({<br>      port_min = number<br>      port_max = number<br>    }))<br>    udp = optional(object({<br>      port_min = number<br>      port_max = number<br>    }))<br>    icmp = optional(object({<br>      type = number<br>      code = number<br>    }))<br>  }))</pre> | <pre>{<br>  "allow_all_inbound": {<br>    "direction": "inbound",<br>    "source": "0.0.0.0/0"<br>  }<br>}</pre> | no |
| <a name="input_unique_name"></a> [unique\_name](#input\_unique\_name) | A unique identifier used as a prefix when naming resources that will be provisioned. Must begin with a letter. | `string` | `"asset-multizone"` | no |
| <a name="input_virtual_private_endpoints"></a> [virtual\_private\_endpoints](#input\_virtual\_private\_endpoints) | Map of virtual private endpoints to be added to transit (name:crn\|target), refer to output of 'ibmcloud is endpoint-gateway-targets' or add the CRN of a service that supports VPE(e.g. icd-redis) | `map(any)` | `{}` | no |
| <a name="input_vpc_tags"></a> [vpc\_tags](#input\_vpc\_tags) | Any tags that you want to associate with your VPC. | `list(string)` | `[]` | no |
| <a name="input_zone_list"></a> [zone\_list](#input\_zone\_list) | A list of the availability zones (AZ) to configure for the VPC. Example: `zone_list = ["1"]` will configure a vpc with only one zone, e.g, us-south-1. A `zone_list = ["2", "3"]` will configure a vpc with zone-2 and zone-3, e.g., us-south-2 and us-south-3. | `list(string)` | <pre>[<br>  "1",<br>  "2",<br>  "3"<br>]</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_acl_ids"></a> [acl\_ids](#output\_acl\_ids) | ID of ACLs created |
| <a name="output_subnets"></a> [subnets](#output\_subnets) | List of subnets associated with this VPC, grouped by tier |
| <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id) | ID of VPC created |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Developing

To set up your local development environment, see steps [here](https://github.ibm.com/GoldenEye/documentation/blob/master/local-dev-setup.md)
