package test

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
	"testing"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/otiai10/copy"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.ibm.com/GoldenEye/common-go/testhelper"
)

/*
The upgrade test works by:
i. running terraform apply against master branch of the module
ii. copying the state to the code from the PR branch
iii. running terraform plan against the PR branch

If any resources are identified in the plan output for destroy, the test will fail
(unless the resource has been added to the IsExemptedResource function).

NOTE: Upgrade tests are skipped if the string 'BREAKING CHANGE' or 'SKIP UPGRADE TEST'
are detected in any of the commit messages.
*/
func TestUpgrade(t *testing.T) {
	// Run your tests in parallel by adding t.Parallel() to the top of each test.
	t.Parallel()

	// Skip upgrade test in continuous testing pipeline which runs in short mode
	if testing.Short() {
		t.Skip("Skipping upgrade test in short mode.")
	}

	// Determine the name of the PR branch
	branchCmd := exec.Command("/bin/sh", "-c", "git rev-parse --abbrev-ref HEAD")
	branch, _ := branchCmd.CombinedOutput()

	// Get all the commit messages from the PR branch
	cmd := exec.Command("/bin/sh", "-c", "git log master..", string(branch))
	out, _ := cmd.CombinedOutput()

	// Skip upgrade test if BREAKING CHANGE OR SKIP UPGRADE TEST string found in commit messages
	var doNotRunUpgradeTest bool
	if strings.Contains(string(out), "BREAKING CHANGE") || strings.Contains(string(out), "SKIP UPGRADE TEST") {
		doNotRunUpgradeTest = true
	}

	if doNotRunUpgradeTest == true {
		fmt.Println("Detected the string \"BREAKING CHANGE\" or \"SKIP UPGRADE TEST\" used in commit message, skipping upgrade test.")
	} else {
		// Verify required environment variables are set - better to do this now rather than retry and fail with every attempt
		checkVariables := []string{"TF_VAR_ibmcloud_api_key", "GH_TOKEN"}
		for i := 0; i < len(checkVariables); i++ {
			checkVariable := checkVariables[i]
			val, present := os.LookupEnv(checkVariable)
			require.True(t, present, checkVariable+" environment variable not set")
			require.NotEqual(t, "", val, checkVariable+" environment variable is empty")
		}

		keyval, _ := os.LookupEnv("TF_VAR_ibmcloud_api_key")

		// Determine temp dir to run all upgrade tests in
		folderPrefix := strings.ToLower(random.UniqueId())
		tmpDir := "/tmp/" + folderPrefix

		// Create folder in temp dir to copy master branch code to
		masterBranchDir := tmpDir + "/master_branch/"
		logger.Log(t, "Creating temp dir:", masterBranchDir)
		os.MkdirAll(masterBranchDir, 0700)

		// Clone code from master branch to temp dir
		tokenVal, _ := os.LookupEnv("GH_TOKEN")
		rootedPath, _ := os.Getwd()
		gitResponse, _ := git.PlainOpen(rootedPath + "/../")
		repoConfig, _ := gitResponse.Config()
		moduleURL := strings.Replace(repoConfig.Remotes["origin"].URLs[0], ":", "/", 1)
		moduleURL = strings.Replace(moduleURL, "git@", "https://", 1)

		logger.Log(t, "Cloning code from master branch..")
		_, errClone := git.PlainClone(masterBranchDir, false, &git.CloneOptions{
			Auth: &http.BasicAuth{ // pragma: allowlist secret
				Username: "abc123", // This can be anything except an empty string
				Password: tokenVal, // pragma: allowlist secret
			},
			URL:      moduleURL,
			Progress: os.Stdout,
		})

		// Hard fail the test if the clone of master branch fails
		if errClone != nil {
			t.Fatalf("Git clone failed. Error: %s", errClone)
		}

		// Unique prefix to avoid clashes
		prefix := fmt.Sprintf("vpc-module-upg-%s", strings.ToLower(random.UniqueId()))

		// Programmatically determine region to use based on availability
		// Set OS environment variable FORCE_TEST_REGION to force a specific region
		region, _ := testhelper.GetBestVpcRegion(keyval, "../common-dev-assets/common-go-assets/cloudinfo-region-vpc-gen2-prefs.yaml", "us-south")

		// Set the path (relative to the root directory) to the Terraform code that will be tested.
		tfDir := "examples/basic"

		// Vars to pass into module
		vars := map[string]interface{}{
			"prefix": prefix,
			"region": region,
		}

		// Construct the terraform options for the master branch apply with default retryable errors to handle
		// the most common retryable errors in terraform testing.
		terraformOptionsMaster := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
			TerraformDir: masterBranchDir + tfDir,
			Vars:         vars,
			// Set Upgrade to true to ensure latest version of providers and modules are used by terratest.
			// This is the same as setting the -upgrade=true flag with terraform.
			Upgrade: true,
		})

		// Run terraform init and apply with the given options against master branch code
		logger.Log(t, "START: Init / Apply (master branch)")
		_, errMasterBranch := terraform.InitAndApplyE(t, terraformOptionsMaster)
		if errMasterBranch != nil {
			assert.True(t, errMasterBranch == nil, "FAIL: Init / Apply (master branch): ", errMasterBranch)
		}
		logger.Log(t, "END: Init / Apply (master branch)")

		// Only proceed to upgrade test of master branch apply passed
		if errMasterBranch == nil {

			// Create folder in temp dir to copy PR code to
			prBranchDir := tmpDir + "/pr_branch/"
			logger.Log(t, "Creating temp dir:", prBranchDir)
			os.MkdirAll(prBranchDir, 0700)

			// Copy PR branch code to temp dir (Do not copy state or cache)
			skipFilesAndFolders := []string{".tfstate", ".terraform", "tfstate.d", "tfstate.backup"}
			logger.Log(t, "Copying PR branch code to: ", prBranchDir)
			opt := copy.Options{
				Skip: func(src string) (bool, error) {
					for _, v := range skipFilesAndFolders {
						if strings.HasSuffix(src, v) {
							return true, nil
						}
					}
					return false, nil
				},
			}
			copy.Copy("../", prBranchDir, opt)

			// Copy master apply state file to PR branch temp dir
			srcFile, _ := os.Open(masterBranchDir + tfDir + "/terraform.tfstate")
			destFile, _ := os.Create(prBranchDir + tfDir + "/terraform.tfstate")
			logger.Log(t, "Copying master state file from "+masterBranchDir+" to "+prBranchDir)
			_, errCopyTFState := io.Copy(destFile, srcFile)

			// Only proceed with upgrade test if copy of state file was a success
			if errCopyTFState != nil {
				assert.True(t, errMasterBranch == nil, "Error copying state file: ", errMasterBranch)
			} else {
				terraformOptionsPR := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
					TerraformDir: prBranchDir + tfDir,
					Vars:         vars,
					// Set Upgrade to true to ensure latest version of providers and modules are used by terratest.
					// This is the same as setting the -upgrade=true flag with terraform.
					Upgrade: true,
				})

				// InitAndPlanAndShowWithStructNoLog runs InitAndPlanAndShowWithStruct without logging.
				// InitAndPlanAndShowWithStruct runs terraform init, then terraform plan, and then terraform show with
				// the given options, and parses the json result into a go struct.
				logger.Log(t, "START: Init / Plan / Show (PR branch)")
				planStruct := terraform.InitAndPlanAndShowWithStructNoLogTempPlanFile(t, terraformOptionsPR)
				logger.Log(t, "END: Init / Plan / Show (PR branch)")

				logger.Log(t, "Parsing plan output to determine if any resources identified for destroy (PR branch)..")
				for _, resource := range planStruct.ResourceChangesMap {
					if !IsExemptedResource(resource.Address) {
						for _, action := range resource.Change.Actions {
							if action == "destroy" {
								assert.False(t, action == "destroy", "Resource(s) identified to be destroyed. Failing test.")
							}
						}
					}
				}
			}
		}

		// Check if "DO_NOT_DESTROY_ON_FAILURE" is set
		envVal, _ := os.LookupEnv("DO_NOT_DESTROY_ON_FAILURE")

		// Do not destroy if tests failed and "DO_NOT_DESTROY_ON_FAILURE" is true
		if t.Failed() && strings.ToLower(envVal) == "true" {
			fmt.Println("Terratest failed. Debug the test and delete resources manually.")
		} else {
			logger.Log(t, "START: Destroy")
			terraform.Destroy(t, terraformOptionsMaster)
			logger.Log(t, "END: Destroy")
			logger.Log(t, "Deleting temp dir:", tmpDir)
			os.RemoveAll(tmpDir)
		}
	}
}

/*
During an upgrade test, we never want terraform to identify any resources to be destroyed.
However, there may be a rare case where we may need to allow this. The below function can
be used to tell the test to ignore a specific resources that has been identified for
destroy.
*/
func IsExemptedResource(resource string) bool {
	switch resource {
	case
		// Exempt a resource you know will be destroyed during upgrade by using the syntax:
		// module.module-name.resource-type.resource-name
		"":
		return true
	}
	return false
}
