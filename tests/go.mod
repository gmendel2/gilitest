module github.ibm.com/GoldenEye/vpc-module

go 1.16

require (
	github.com/go-git/go-git/v5 v5.4.2
	github.com/gruntwork-io/terratest v0.40.6
	github.com/otiai10/copy v1.7.0
	github.com/stretchr/testify v1.7.1
	github.ibm.com/GoldenEye/common-go v1.6.0
)
