# Tests

For information on how to create and run tests, see the following
[guide](https://github.ibm.com/GoldenEye/documentation/blob/master/tests.md).

<!-- Include any extra steps not in above guide that may be specific to tests in this project -->
