package test

import (
	"fmt"
	"os"
	"strings"
	"testing"

	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.ibm.com/GoldenEye/common-go/testhelper"
)

func TestDefaultExample(t *testing.T) {
	// Run your tests in parallel by adding t.Parallel() to the top of each test.
	t.Parallel()

	// Unique prefix to avoid clashes
	prefix := fmt.Sprintf("vpc-module-%s", strings.ToLower(random.UniqueId()))

	// Verify ibmcloud_api_key variable is set - better to do this now rather than retry and fail with every attempt
	checkVariable := "TF_VAR_ibmcloud_api_key"
	val, present := os.LookupEnv(checkVariable)
	require.True(t, present, checkVariable+" environment variable not set")
	require.NotEqual(t, "", val, checkVariable+" environment variable is empty")

	// Programmatically determine region to use based on availability
	// Set OS environment variable FORCE_TEST_REGION to force a specific region
	region, _ := testhelper.GetBestVpcRegion(val, "../common-dev-assets/common-go-assets/cloudinfo-region-vpc-gen2-prefs.yaml", "us-south")

	// Set the relative path (from the tests directory) to the Terraform code that will be tested.
	tfDir := "../examples/basic"

	// Construct the terraform options with default retryable errors to handle the most common
	// retryable errors in terraform testing.
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		// Set the path to the Terraform code that will be tested.
		TerraformDir: tfDir,
		Vars: map[string]interface{}{
			"prefix": prefix,
			"region": region,
		},
		// Set Upgrade to true to ensure latest version of providers and modules are used by terratest.
		// This is the same as setting the -upgrade=true flag with terraform.
		Upgrade: true,
	})

	// Always run in a new clean workspace to avoid reusing existing state files
	terraform.WorkspaceSelectOrNew(t, terraformOptions, prefix)

	// Run terraform init and apply with the given options and return stdout/stderr from the apply command.
	// It then runs plan again and will fail the test if plan requires additional changes.
	logger.Log(t, "START: Init / Apply / Idempotent Check")
	_, err := terraform.InitAndApplyAndIdempotentE(t, terraformOptions)
	if err != nil {
		assert.True(t, err == nil, "FAIL: Init / Apply / Idempotent Check: ", err)
	} else {
		// Run `terraform output` to get the values of output variables and check they have the expected values.
		output := terraform.Output(t, terraformOptions, "vpc")
		assert.NotNil(t, output)
		logger.Log(t, "END: Init / Apply / Idempotent Check")
	}

	// Check if "DO_NOT_DESTROY_ON_FAILURE" is set
	envVal, _ := os.LookupEnv("DO_NOT_DESTROY_ON_FAILURE")

	// Do not destroy if tests failed and "DO_NOT_DESTROY_ON_FAILURE" is true
	if t.Failed() && strings.ToLower(envVal) == "true" {
		fmt.Println("Terratest failed. Debug the test and delete resources manually.")
	} else {
		logger.Log(t, "START: Destroy")
		terraform.Destroy(t, terraformOptions)
		terraform.WorkspaceDelete(t, terraformOptions, prefix)
		logger.Log(t, "END: Destroy")
	}
}
