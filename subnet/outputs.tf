##############################################################################
# Outputs
##############################################################################
output "subnet_ids" {
  description = "IDs of subnets created for this tier"
  value       = ibm_is_subnet.subnet.*.id
}

output "subnets" {
  description = "Subnets in this tier"
  value       = [for v in ibm_is_subnet.subnet.* : { id = v.id, zone = v.zone, cidr_block = v.ipv4_cidr_block }]
}

##############################################################################
