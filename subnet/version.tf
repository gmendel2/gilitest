terraform {
  required_version = ">= 1.0.0"
  experiments      = [module_variable_optional_attrs]
  required_providers {
    # Pin to the lowest provider version of the range defined in the main module to ensure lowest version still works
    ibm = {
      source = "ibm-cloud/ibm"
      # Use a range in modules
      version = ">= 1.36.0"
    }
  }
}
