# Subnet module

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.0 |
| <a name="requirement_ibm"></a> [ibm](#requirement\_ibm) | >= 1.36.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [ibm_is_subnet.subnet](https://registry.terraform.io/providers/ibm-cloud/ibm/latest/docs/resources/is_subnet) | resource |
| [ibm_is_vpc_address_prefix.subnet_prefix](https://registry.terraform.io/providers/ibm-cloud/ibm/latest/docs/resources/is_vpc_address_prefix) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_acl_id"></a> [acl\_id](#input\_acl\_id) | ID of ACL for subnets to use | `string` | n/a | yes |
| <a name="input_cidr_blocks"></a> [cidr\_blocks](#input\_cidr\_blocks) | CIDR blocks for subnets to be created. If no CIDR blocks are provided, it will create subnets with 256 total ipv4 addresses | `list(string)` | <pre>[<br>  "10.10.10.0/24",<br>  "10.10.11.0/24",<br>  "10.10.12.0/24"<br>]</pre> | no |
| <a name="input_enable_acl_id"></a> [enable\_acl\_id](#input\_enable\_acl\_id) | Enables acl id if true | `bool` | `true` | no |
| <a name="input_ibm_region"></a> [ibm\_region](#input\_ibm\_region) | Region for resources to be created | `string` | n/a | yes |
| <a name="input_public_gateways"></a> [public\_gateways](#input\_public\_gateways) | List of public gateway ids | `list(string)` | `[]` | no |
| <a name="input_resource_group"></a> [resource\_group](#input\_resource\_group) | Name of resource group to create VPC | `string` | `"asset-development"` | no |
| <a name="input_unique_id"></a> [unique\_id](#input\_unique\_id) | Unique ID for subnets created | `string` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | ID of VPC where subnet needs to be created | `string` | n/a | yes |
| <a name="input_zones"></a> [zones](#input\_zones) | A list of zone numbers to deploy subnets into. A zone number maps directly to the target region's zone number. | `list(string)` | <pre>[<br>  "1",<br>  "2",<br>  "3"<br>]</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_subnet_ids"></a> [subnet\_ids](#output\_subnet\_ids) | IDs of subnets created for this tier |
| <a name="output_subnets"></a> [subnets](#output\_subnets) | Subnets in this tier |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
