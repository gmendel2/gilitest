##############################################################################
# Prefixes and subnets
#
# Creates a number of address prefixes per zone equal to the subnets per
# zone times the number of zones
##############################################################################

resource "ibm_is_vpc_address_prefix" "subnet_prefix" {
  count = length(var.cidr_blocks) > 0 ? length(var.zones) : 0
  name  = "${var.unique_id}-z-${var.zones[count.index]}"
  zone  = "${var.ibm_region}-${var.zones[count.index]}"
  vpc   = var.vpc_id
  cidr  = element(var.cidr_blocks, count.index)
}

##############################################################################


##############################################################################
# Create Subnets
#
# Creates a subnet for each tier/zone combination
##############################################################################

resource "ibm_is_subnet" "subnet" {
  count                    = length(var.zones)
  name                     = "${var.unique_id}-subnet-${var.zones[count.index]}"
  vpc                      = var.vpc_id
  resource_group           = var.resource_group
  zone                     = "${var.ibm_region}-${var.zones[count.index]}"
  ipv4_cidr_block          = length(var.cidr_blocks) > 0 ? element(ibm_is_vpc_address_prefix.subnet_prefix.*.cidr, count.index) : null
  network_acl              = var.enable_acl_id ? var.acl_id : null
  public_gateway           = length(var.public_gateways) > 0 ? element(var.public_gateways, count.index) : null
  total_ipv4_address_count = length(var.cidr_blocks) > 0 ? null : 256
}

##############################################################################
