##############################################################################
# Account Variables
##############################################################################

variable "unique_name" {
  description = "A unique identifier used as a prefix when naming resources that will be provisioned. Must begin with a letter."
  type        = string
  default     = "asset-multizone"
}

variable "ibm_region" {
  description = "IBM Cloud region where all resources will be deployed"
  type        = string
}

variable "resource_group_id" {
  description = "ID of resource group to use when creating the VPC"
  type        = string
}

##############################################################################


##############################################################################
# Network variables
##############################################################################

variable "classic_access" {
  description = "Enable VPC Classic Access. Note: only one VPC per region can have classic access"
  type        = bool
  default     = false
}

variable "enable_public_gateway" {
  description = "Enable public gateways for private subnets, true or false"
  type        = bool
  default     = true
}

variable "cidr_bases" {
  description = "A list of base CIDR blocks for each network zone"
  type        = map(string)
  default = {
    private = "192.168.0.0/20",
    transit = "192.168.16.0/20",
    edge    = "192.168.32.0/20",
    nlb     = "192.168.48.0/20"
  }
}

variable "new_bits" {
  description = "Number of additional address bits to use for numbering the new networks"
  type        = number
  default     = 2
}

variable "zone_list" {
  description = "A list of the availability zones (AZ) to configure for the VPC. Example: `zone_list = [\"1\"]` will configure a vpc with only one zone, e.g, us-south-1. A `zone_list = [\"2\", \"3\"]` will configure a vpc with zone-2 and zone-3, e.g., us-south-2 and us-south-3."
  type        = list(string)
  default     = ["1", "2", "3"]
  validation {
    condition     = alltrue([for zone in var.zone_list : contains(["1", "2", "3"], zone)])
    error_message = "A zone_list value must be a string of \"1\", \"2\", or \"3\"."
  }
  validation {
    condition     = length(var.zone_list) == length(distinct(var.zone_list))
    error_message = "A zone_list value must be unique string of \"1\", \"2\", or \"3\" within the list."
  }
  validation {
    condition     = length(var.zone_list) >= 1 && length(var.zone_list) <= 3
    error_message = "The zone_list only allows for specifying a minimum of one zone up to a maximum of three zones."
  }
}

variable "vpc_tags" {
  type        = list(string)
  description = "Any tags that you want to associate with your VPC."
  default     = []
}

variable "acl_rules_map" {
  description = "Access control list rule set per network zone"
  type = map(list(object({
    name        = string
    action      = string
    source      = string
    destination = string
    direction   = string
    tcp = optional(object({
      source_port_min = number
      source_port_max = number
      port_min        = number
      port_max        = number
    }))
    udp = optional(object({
      source_port_min = number
      source_port_max = number
      port_min        = number
      port_max        = number
    }))
    icmp = optional(object({
      type = number
      code = number
    }))
  })))
  default = {
    private = [
      {
        name        = "iks-create-worker-nodes-inbound"
        action      = "allow"
        source      = "161.26.0.0/16"
        destination = "0.0.0.0/0"
        direction   = "inbound"
      },
      {
        name        = "iks-nodes-to-master-inbound"
        action      = "allow"
        source      = "166.8.0.0/14"
        destination = "0.0.0.0/0"
        direction   = "inbound"
      },
      {
        name        = "iks-create-worker-nodes-outbound"
        action      = "allow"
        source      = "0.0.0.0/0"
        destination = "161.26.0.0/16"
        direction   = "outbound"
      },
      {
        name        = "iks-worker-to-master-outbound"
        action      = "allow"
        source      = "0.0.0.0/0"
        destination = "166.8.0.0/14"
        direction   = "outbound"
      },
      {
        name        = "allow-all-https-inbound"
        source      = "0.0.0.0/0"
        action      = "allow"
        destination = "0.0.0.0/0"
        direction   = "inbound"
        tcp = {
          source_port_min = 443
          source_port_max = 443
          port_min        = 1
          port_max        = 65535
        }
      },
      {
        name        = "allow-all-https-outbound"
        source      = "0.0.0.0/0"
        action      = "allow"
        destination = "0.0.0.0/0"
        direction   = "outbound"
        tcp = {
          source_port_min = 1
          source_port_max = 65535
          port_min        = 443
          port_max        = 443
        }
      },
      {
        name        = "deny-all-outbound"
        action      = "deny"
        source      = "0.0.0.0/0"
        destination = "0.0.0.0/0"
        direction   = "outbound"
      },
      {
        name        = "deny-all-inbound"
        action      = "deny"
        source      = "0.0.0.0/0"
        destination = "0.0.0.0/0"
        direction   = "inbound"
      }
    ]
  }
}

variable "security_group_rules" {
  description = "List of security group rules to be added to default security group"
  type = map(object({
    source    = string
    direction = string
    tcp = optional(object({
      port_min = number
      port_max = number
    }))
    udp = optional(object({
      port_min = number
      port_max = number
    }))
    icmp = optional(object({
      type = number
      code = number
    }))
  }))
  default = {
    allow_all_inbound = {
      source    = "0.0.0.0/0"
      direction = "inbound"
    }
  }
}

variable "virtual_private_endpoints" {
  description = "Map of virtual private endpoints to be added to transit (name:crn|target), refer to output of 'ibmcloud is endpoint-gateway-targets' or add the CRN of a service that supports VPE(e.g. icd-redis)"
  type        = map(any)
  default     = {}
}
##############################################################################
