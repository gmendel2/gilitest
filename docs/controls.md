# NIST controls

This module implements the following NIST security and privacy controls. For more information about the NIST controls, see the NIST Risk Management Framework [control families](https://csrc.nist.gov/Projects/risk-management/sp800-53-controls/release-search#/families?version=4.0).

## Required controls for compliance

These controls are required for compliance with the listed frameworks.

### Control mapping

The controls that are implemented by this module fall into the following FedRAMP impact levels:

- Moderate impact level: SC-7(3), SC-7(4), SC-7(5), SC-7(8), SC-7(18), SC-8, SC-8(1)
- High impact level: SC-7(3), SC-7(4), SC-7(5), SC-7(8), SC-7(18), SC-7(20), SC-7(21)

### Implementation information

Implementation information for the supported control enhancements.

#### System and communications protection family

Supported controls in the system and communications protection family.

- SC-7(3) BOUNDARY PROTECTION | ACCESS POINTS
    - Limit the number of external network connections to the information system.
    - Implementation
        - Only the edge network is attached to the public network, no workload runs in that network, only proxies.
        - Only the transit network is attached to the private network, no workload runs in that network, only proxies.
- SC-7(4) BOUNDARY PROTECTION | EXTERNAL TELECOMMUNICATIONS SERVICES
    - Edge and transit networks function as the control points for external services.
    - Implementation
        - Edge and transit serve as managed interface for external connections.
        - Edge and transit enforce flow policy for the workload.
        - The service mesh or proxy allows only TLS connections.
        - Proxy configuration in edge and VPE config in transit is the codified list of exceptions, managed through Terraform.
        - Flow logs on the VPC allow for detection of unauthorized traffic. Requires delivery from IBM Kubernetes Service.
        - No network path to or from the workload is included except by a machine in the edge or transit network.
- SC-7(5) BOUNDARY PROTECTION | DENY BY DEFAULT - ALLOW BY EXCEPTION
    - Deny by Default, unless allowed no external connection can be made by the workload.
    - Implementation
        - Unless allowed in the proxy, no connection by hostname is possible to external systems.
        - Unless a VPE is added to transit, no connection to an internal service is possible. Potentially, a proxy can be run in transit.
- SC-7(8) BOUNDARY PROTECTION | ROUTE TRAFFIC TO AUTHENTICATED PROXY SERVERS
    - Route traffic to authenticated proxy servers. Traffic to public must traverse egress gateway in edge.
    - Implementation
        - Traffic to the public needs to traverse a proxy that runs in edge, either service mesh egress gateway or proxy.
        - Same can be done for transit
- SC-7(18)	BOUNDARY PROTECTION | FAIL SECURE
    - Fail Secure. Failure of calico does not lead to unintended system behavior.
    - Implementation
        - Failure of calico cannot compromise system security.
        - Accidental scheduling of workload onto edge is prevented by Kubernetes (Evidence from SCC/Auditree?).
- SC-7(20) BOUNDARY PROTECTION | DYNAMIC ISOLATION / SEGREGATION
    - Dynamic isolation capability.
    - Implementation
        - Use multiple VPC module instances to isolate the control plane and data plane.
- SC-7(21) BOUNDARY PROTECTION | ISOLATION OF SYSTEM COMPONENTS
    - Isolation of system components.
    - Implementation
        - Edge subnet is dedicated as DMZ from to public.
        - Transit subnet is dedicated as DMZ from to other IBM services.
        - Multiple instances of VPC (the data plane or control plane) represent network-isolated slices. See SC-7(20).

## Additional controls

Supported controls that are not required for compliance with FedRamp.

- AC-4(2) DENIAL-OF-SERVICE PROTECTION | DETECTION AND MONITORING
    - Processing Domains. VPCs and Subnets are separate processing domains with distinct security attributes.
- AC-4(4) DENIAL-OF-SERVICE PROTECTION | DETECTION AND MONITORING
    - Flow Control of Encrypted Information. Future versions might configure egress and ingress gateways to enable deep packet inspection.
- AC-4(6) DENIAL-OF-SERVICE PROTECTION | DETECTION AND MONITORING
    - Metadata. Unless allowed by the egress gateway, no flow is possible, egress configuration serves as metadata.
- AC-6(4) LEAST PRIVILEGE | separate PROCESSING DOMAINS
    - Separate Processing Domains. see AC-4(2).
- SC-5(1) DENIAL-OF-SERVICE PROTECTION | RESTRICT ABILITY TO ATTACK OTHER SYSTEMS
    - Restrict the ability to attack other systems. Public and private ingress is separated by subnet.
- SC-5(2) DENIAL-OF-SERVICE PROTECTION | CAPACITY, BANDWIDTH, AND REDUNDANCY
    - Capacity, Bandwidth, Redundancy. No attack on public can affect private endpoints.
- SC-5(3) DENIAL-OF-SERVICE PROTECTION | DETECTION AND MONITORING
    - Detection and Monitoring. Future versions might use flow logs for monitoring and anomaly detection.
- SC-7(22) BOUNDARY PROTECTION | SEPARATE SUBNETS FOR CONNECTING TO DIFFERENT SECURITY DOMAINS
    - By using multiple instances of the module, separate subnets are implemented to connect to different security domains in a double DMZ design.
