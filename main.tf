##############################################################################
# This file creates the VPC, Zones, subnets and public gateway for the VPC
# a separate file sets up the load balancers, listeners, pools and members
##############################################################################

resource "null_resource" "subnet_mappings" {
  count = length(var.zone_list)

  triggers = {
    name     = "zone-${var.zone_list[count.index]}"
    new_bits = var.new_bits
  }
}

module "zone_subnet_addrs" {
  source   = "git::https://github.com/hashicorp/terraform-cidr-subnets.git?ref=v1.0.0"
  for_each = var.cidr_bases

  base_cidr_block = each.value

  networks = null_resource.subnet_mappings.*.triggers
}

##############################################################################
# Create a VPC
##############################################################################

resource "ibm_is_vpc" "vpc" {
  name                        = "${var.unique_name}-vpc"
  resource_group              = var.resource_group_id
  classic_access              = var.classic_access
  default_network_acl_name    = "${var.unique_name}-edge-acl"
  default_security_group_name = "${var.unique_name}-default-sg"
  default_routing_table_name  = "${var.unique_name}-default-table"
  address_prefix_management   = "manual"
  tags                        = var.vpc_tags
}

##############################################################################


##############################################################################
# Public Gateways (Optional)
##############################################################################

resource "ibm_is_public_gateway" "gateway" {
  count          = var.enable_public_gateway ? length(var.zone_list) : 0
  name           = "${var.unique_name}-gateway-${var.zone_list[count.index]}"
  vpc            = ibm_is_vpc.vpc.id
  resource_group = var.resource_group_id
  zone           = "${var.ibm_region}-${var.zone_list[count.index]}"
}

##############################################################################


##############################################################################
# Multizone subnets
##############################################################################

module "vpc_subnets" {
  for_each        = var.cidr_bases
  source          = "./subnet"
  ibm_region      = var.ibm_region
  zones           = var.zone_list
  unique_id       = "${var.unique_name}-${each.key}"
  acl_id          = lookup(ibm_is_network_acl.multizone_acl, each.key, { id : null }).id
  cidr_blocks     = values(module.zone_subnet_addrs[each.key].network_cidr_blocks)
  vpc_id          = ibm_is_vpc.vpc.id
  resource_group  = var.resource_group_id
  public_gateways = each.key == "edge" ? ibm_is_public_gateway.gateway.*.id : []
}

##############################################################################


##############################################################################
# Virtual Endpoint Gateways
##############################################################################

resource "ibm_is_virtual_endpoint_gateway" "endpoint_gateways" {
  for_each   = var.virtual_private_endpoints
  depends_on = [module.vpc_subnets]
  name       = "${var.unique_name}-vpe-${each.key}"
  # check if target is a CRN and handle accordingly
  target {
    name          = length(regexall("crn:v1:([^:]*:){6}", each.value)) > 0 ? null : each.value
    crn           = length(regexall("crn:v1:([^:]*:){6}", each.value)) > 0 ? each.value : null
    resource_type = length(regexall("crn:v1:([^:]*:){6}", each.value)) > 0 ? "provider_cloud_service" : "provider_infrastructure_service"
  }
  vpc = ibm_is_vpc.vpc.id
  dynamic "ips" {
    for_each = module.vpc_subnets["transit"].subnet_ids
    content {
      subnet = ips.value
      name   = "${var.unique_name}-vpe-${each.key}-${index(module.vpc_subnets["transit"].subnet_ids, ips.value)}"
    }
  }
  resource_group = var.resource_group_id
}


##############################################################################
